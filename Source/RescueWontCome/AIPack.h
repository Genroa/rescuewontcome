// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "MobInterface.h"
#include "AIPack.generated.h"



/**
* Struct used to define the content of a pack, the definition might get extended with new monsters
*/
USTRUCT(BlueprintType)
struct FPackDescriptor
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Monsters")
	int zerglingCount;
};

/**
 * Class 
 */
UCLASS(BlueprintType)
class RESCUEWONTCOME_API UAIPack : public UObject
{
	GENERATED_BODY()
public:
	/// <summary>
	/// Use as a late initializer, the descriptor is used as the list of mob that the pack will contain and the pos is the spawn position of the pack (used also for target selection)
	/// </summary>
	UFUNCTION(BlueprintCallable)
	void SetupPack(const FPackDescriptor& descript, const FVector& pos);


	/// <summary>
	/// Launch the pack on the selected target, target is selected at launch not on setup
	/// </summary>
	UFUNCTION(BlueprintCallable)
	void Launch();


	/// <summary>
	/// A list of all the mobs in the pack
	/// </summary>
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Monsters")
	TArray<TScriptInterface<UMobInterface>> Mobs;

private:
	FPackDescriptor PackDescript;
	FVector position;
};
