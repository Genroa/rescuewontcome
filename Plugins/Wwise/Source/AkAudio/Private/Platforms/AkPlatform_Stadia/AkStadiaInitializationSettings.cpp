// Copyright (c) 2006-2019 Audiokinetic Inc. / All Rights Reserved

#include "Platforms/AkPlatform_Stadia/AkStadiaInitializationSettings.h"
#include "AkAudioDevice.h"

void UAkStadiaInitializationSettings::FillInitializationStructure(FAkInitializationStructure& InitializationStructure) const
{
	InitializationStructure.SetupLLMAllocFunctions();

	CommonSettings.FillInitializationStructure(InitializationStructure);
	CommunicationSettings.FillInitializationStructure(InitializationStructure);
	AdvancedSettings.FillInitializationStructure(InitializationStructure);
}

