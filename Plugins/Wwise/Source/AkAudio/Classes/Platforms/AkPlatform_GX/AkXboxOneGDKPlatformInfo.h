#pragma once

#include "Platforms/AkPlatformInfo.h"
#include "AkXboxOneGDKPlatformInfo.generated.h"

UCLASS()
class UAkXboxOneGDKPlatformInfo : public UAkPlatformInfo
{
	GENERATED_BODY()

public:
	UAkXboxOneGDKPlatformInfo()
	{
		WwisePlatform = "XboxOneGDK";

#ifdef AK_GX_VS_VERSION
		Architecture = "GX_" AK_GX_VS_VERSION;
#else
		Architecture = "GX_vc150";
#endif

		LibraryFileNameFormat = "{0}.dll";
		DebugFileNameFormat = "{0}.pdb";

#if WITH_EDITOR
		// XboxOneGDK uses the XboxOne sound data.
		UAkPlatformInfo::UnrealNameToWwiseName.Add("XboxOneGDK", "XboxOne");
#endif
	}
};

UCLASS()
class UAkXboxOneAnvilPlatformInfo : public UAkXboxOneGDKPlatformInfo
{
	GENERATED_BODY()
		UAkXboxOneAnvilPlatformInfo()
	{
#if WITH_EDITOR
		// XboxOneAnvil uses the XboxOne sound data.
		UAkPlatformInfo::UnrealNameToWwiseName.Add("XboxOneAnvil", "XboxOne");
#endif
	}
};
