#pragma once

#ifdef AK_GDX

#include "Platforms/AkPlatformBase.h"
#include "AkWinGDKInitializationSettings.h"

#define TCHAR_TO_AK(Text) (const WIDECHAR*)(Text)

using UAkInitializationSettings = UAkWinGDKInitializationSettings;

struct FAkGDXPlatform : FAkPlatformBase
{
	static const UAkInitializationSettings* GetInitializationSettings()
	{
		return GetDefault<UAkWinGDKInitializationSettings>();
	}

	static const FString GetPlatformBasePath()
	{
		return FString("Windows");
	}
};

using FAkPlatform = FAkGDXPlatform;

#endif
