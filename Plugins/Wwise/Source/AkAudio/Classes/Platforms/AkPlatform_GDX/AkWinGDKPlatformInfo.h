#pragma once

#include "Platforms/AkPlatformInfo.h"
#include "AkWinGDKPlatformInfo.generated.h"

UCLASS()
class UAkWinGDKPlatformInfo : public UAkPlatformInfo
{
	GENERATED_BODY()

public:
	UAkWinGDKPlatformInfo()
	{
		WwisePlatform = "Windows";

#ifdef AK_GDX_VS_VERSION
		Architecture = "GDX_" AK_GDX_VS_VERSION;
#else
		Architecture = "GDX_vc150";
#endif

		LibraryFileNameFormat = "{0}.dll";
		DebugFileNameFormat = "{0}.pdb";

#if WITH_EDITOR
		UAkPlatformInfo::UnrealNameToWwiseName.Add("WinGDK", "Windows");
#endif
	}
};

UCLASS()
class UAkWinAnvilPlatformInfo : public UAkWinGDKPlatformInfo
{
	GENERATED_BODY()
	UAkWinAnvilPlatformInfo()
	{
#if WITH_EDITOR
		UAkPlatformInfo::UnrealNameToWwiseName.Add("WinAnvil", "Windows");
#endif
	}
};
