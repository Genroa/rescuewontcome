#pragma once

#if defined(PLATFORM_STADIA) && PLATFORM_STADIA

#include "Platforms/AkPlatformBase.h"
#include "AkUEFeatures.h"
#include "AkStadiaInitializationSettings.h"

#define TCHAR_TO_AK(Text) (const ANSICHAR*)(TCHAR_TO_ANSI(Text))
#if UE_4_24_OR_LATER
using UAkInitializationSettings = UAkStadiaInitializationSettings;
#else
using UAkInitializationSettings = UAkQuailInitializationSettings;
#endif

struct FAkStadiaPlatform : FAkPlatformBase
{
	static const UAkInitializationSettings* GetInitializationSettings()
	{
		return GetDefault<UAkInitializationSettings>();
	}

	static const FString GetPlatformBasePath()
	{
		return FString("Stadia");
	}
};

using FAkPlatform = FAkStadiaPlatform;

#endif
