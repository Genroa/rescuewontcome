#pragma once

#include "Platforms/AkPlatformInfo.h"
#include "AkUEFeatures.h"
#include "AkStadiaPlatformInfo.generated.h"

UCLASS()
class UAkStadiaPlatformInfo : public UAkPlatformInfo
{
	GENERATED_BODY()

public:
	UAkStadiaPlatformInfo()
	{
		WwisePlatform = "Stadia";
		Architecture = "GGP";
		LibraryFileNameFormat = "{0}.so";

#if WITH_EDITOR
		UAkPlatformInfo::UnrealNameToWwiseName.Add("Stadia", "Stadia");
#endif
	}
};

UCLASS()
class UAkQuailPlatformInfo : public UAkStadiaPlatformInfo
{
	GENERATED_BODY()
	UAkQuailPlatformInfo()
	{
#if WITH_EDITOR
		UAkPlatformInfo::UnrealNameToWwiseName.Add("Quail", "Stadia");
#endif
	}
};
